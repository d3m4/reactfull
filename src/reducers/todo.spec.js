import reducer from './todo'

describe('Todo Reducer', () => {
  test('returs a state object', () => {
    const result = reducer(undefined, {type: 'ANYTHING'})
    expect(result).toBeDefined()
  })

  test('adds a todo', () => {
    const startState = {
      todos: [
        {id: 1, name: 'Create static UI', isComplete: true},
        {id: 2, name: 'Create initial State', isComplete: true},
        {id: 3, name: 'Use state to render UI', isComplete: true},
      ]
    }

    const expectedState = {
      todos: [
        {id: 1, name: 'Create static UI', isComplete: true},
        {id: 2, name: 'Create initial State', isComplete: true},
        {id: 3, name: 'Use state to render UI', isComplete: true},
        {id: 4, name: 'Added todo', isComplete: false}
      ]
    }

    const action = {
      type:'TODO_ADD',
      payload: {id: 4, name: 'Added todo', isComplete: false}
    }

    const result = reducer(startState, action)
    expect(result).toEqual(expectedState)
  })
})
